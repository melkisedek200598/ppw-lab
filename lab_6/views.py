from django.shortcuts import render

# Create your views here.
response = {}
def index(request):
	response['author'] = "Melkisedek Bernadius Sibarani"
	html = 'lab_6/lab_6.html'
	return render(request, html, response)
